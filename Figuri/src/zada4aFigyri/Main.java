package zada4aFigyri;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.print("Введите длину основания фигуры:");
        int side = in.nextInt();
        System.out.print("Введите высоту фигуры:");
        int height = in.nextInt();

        Triangle triangle = new Triangle(side,height);
        System.out.println("Площадь треугольника:" + triangle.getArea());


        Parallelogram parallelogram = new Parallelogram(side,height);
        System.out.println("Площадь параллелограмма:" + parallelogram.getArea());

    }
}
