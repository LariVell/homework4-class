package zada4aFigyri;

public class Parallelogram extends Figuri {

    public Parallelogram( int side, int height) {
        super(side,height);
    }

    @Override
    public double getArea() {
        double area = (getSide()*getHeight());
        return area;
    }

}
