package zada4aFigyri;

public class Triangle extends Figuri {

    public Triangle( int side, int height) {
        super(side,height);
    }

    @Override
    public double getArea() {
        double area = (getSide()*getHeight())/2;
        return area;
    }

}
