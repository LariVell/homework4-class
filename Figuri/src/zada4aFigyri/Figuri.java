package zada4aFigyri;

public  abstract class Figuri {
    private int mSide;
    private int mHeight;

    public Figuri ( int side, int height) {
        mSide = side;
        mHeight = height;
    }
    public double getSide(){
        return mSide;
    }
    public double getHeight() {
        return mHeight;
    }

    public abstract double getArea();

}
